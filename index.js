function countLetter(letter, sentence) {

// Check first whether the letter is a single character.
// If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
// If letter is invalid, return undefined.
  if (typeof letter !== 'string' || letter.length !== 1) {
    return undefined;
  }

  let count = 0;
  for (let i = 0; i < sentence.length; i++) {
    if (sentence[i] === letter) {
      count++;
    }
  }
  return count;
}

function isIsogram(text) {
     // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
  text = text.toLowerCase();
  const letters = {};
  for (let i = 0; i < text.length; i++) {
    if (text[i] >= 'a' && text[i] <= 'z') {
         if (letters[text[i]]) {
        return false;
      } else {
               letters[text[i]] = true;
      }
    }
  }
  return true;
}

function purchase(age, price) {
  if (age < 13) {
    return undefined;
  } else if (age >= 13 && age <= 21) {
    return String(Math.round(price * 0.80));
  } else if (age >= 65) {
    return String(Math.round(price * 0.80));
  } else {
    return String(Math.round(price));
  }
}









function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    
    const hotCategories = [];
    const categories = new Set();
    
    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        
        if (item.stocks === 0 && !categories.has(item.category)) {
            hotCategories.push(item.category);
            categories.add(item.category);
        }
    }
    
    return hotCategories;
}

const items = [    { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },    { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },    { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },    { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },    { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }];

console.log(findHotCategories(items)); 


function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
const voters = {};
  const result = [];

  for (let i = 0; i < candidateA.length; i++) {
    if (candidateB.includes(candidateA[i])) {
      if (!voters[candidateA[i]]) {
        voters[candidateA[i]] = true;
        result.push(candidateA[i]);
      }
    }
  }

  return result;

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};